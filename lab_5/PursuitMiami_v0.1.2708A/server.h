#define _WINSOCK_DEPRECATED_NO_WARNINGS
#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib, "Ws2_32.lib")


#include <sdkddkver.h>
#include <stdio.h>
#include <conio.h>

#include <Windows.h>
#include <iostream>
#include <string>
#include <thread>

#define SCK_VERSION2 0x0202

class Proxy {
public:
	virtual void run() = 0;
	virtual int com(const int& a) = 0;
};

class Client : public Proxy {
private:
	int request = 0;
	int response = 0;
public:
	void proceed();
	void run() override;
	int com(const int& a) override;
};

class Server : public Proxy {
private:
	int request = 0;
	int response = 0;
public:

	void proceed();
	void run() override;
	int com(const int& a) override;
};
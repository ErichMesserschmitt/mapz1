#include "server.h"

using namespace std;

void Client::proceed() {
	long SUCCESFULL;
	WSADATA WinSockData;
	WORD DLLVERSION;

	DLLVERSION = MAKEWORD(2, 1);
	SUCCESFULL = WSAStartup(DLLVERSION, &WinSockData);
	SOCKADDR_IN ADRESS;


	string CONVERTER;
	char message[256];

	SOCKET sock;
	sock = socket(AF_INET, SOCK_STREAM, NULL);
	ADRESS.sin_addr.s_addr = inet_addr("127.0.0.1");
	ADRESS.sin_family = AF_INET;
	ADRESS.sin_port = htons(9002);
	while (13) {
		connect(sock, (SOCKADDR*)&ADRESS, sizeof(ADRESS));


		SUCCESFULL = send(sock, std::string(std::to_string(request)).data(), 256, NULL);
		SUCCESFULL = recv(sock, message, 256, NULL);
		response = std::stoi(string(message));
	}
}

void Client::run() {
	std::thread thr(&Client::proceed, this);
	thr.detach();
}

int Client::com(const int& a) {
	this->request = a;
	return response;
}


void Server::proceed() {
	long SUCCESFULL;
	WSADATA WinSockData;
	WORD DLLVERSION;
	DLLVERSION = MAKEWORD(2, 1);
	SUCCESFULL = WSAStartup(DLLVERSION, &WinSockData);
	SOCKADDR_IN ADRESS;
	int AdressSize = sizeof(ADRESS);
	SOCKET Sock_LISTEN;
	SOCKET Sock_CONNECTION;
	Sock_CONNECTION = socket(AF_INET, SOCK_STREAM, NULL);
	ADRESS.sin_addr.S_un.S_addr = INADDR_ANY;
	ADRESS.sin_family = AF_INET;
	ADRESS.sin_port = htons(9002);
	if (Sock_CONNECTION == -1)
		cout << "Something wrong with creating a socket\n\n";

	Sock_LISTEN = socket(AF_INET, SOCK_STREAM, NULL);
	bind(Sock_LISTEN, (SOCKADDR*)&ADRESS, sizeof(ADRESS));
	listen(Sock_LISTEN, SOMAXCONN);
	if (Sock_LISTEN == -1)
		cout << "Something wrong with creating a socket\n\n";

	while (13) {
		cout << "SERVER: Waiting for connection\n";
		cout << ADRESS.sin_addr.s_addr;
		ADRESS.sin_addr.S_un.S_addr = INADDR_ANY;
		cout << "\n";
		if (Sock_CONNECTION = accept(Sock_LISTEN, (SOCKADDR*)&ADRESS, &AdressSize)) {
			cout << "\n" << Sock_CONNECTION << "\n\n";
			cout << "\t\tConnection was found. Enter your message\n";
			char message[256];
			while (1) {
				SUCCESFULL = send(Sock_CONNECTION, std::string(std::to_string(request)).data(), 256, NULL);
				SUCCESFULL = recv(Sock_CONNECTION, message, 256, NULL);
				response = std::stoi(string(message));
				cout << "Server sent an information: " << message << "\n";
			}
		}
	}
}

void Server::run() {

	std::thread thr(&Server::proceed, this);
	Sleep(4999);
	thr.detach();
}

int Server::com(const int& a) {
	this->request = a;
	return response;
}


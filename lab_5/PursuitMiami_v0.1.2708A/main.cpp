#define OLC_PGE_APPLICATION
#include "pixelengine.h"
#include <string>
#include <cstdlib>
#include <stdio.h>
#include <conio.h>
#include <chrono>
#include <time.h>
#include "pcar.h"
#include "server.h"

using namespace std;






class MainWindow : public olc::PixelGameEngine {
private:
	PCar Car1;
	PCar Car2;
	CareTaker* quicksaves;
	Context* context;
	Context* context2;
	int car2pos = 0;
	int car1pos = 0;
	string str_rpm_current;
	string str_speed_current;
	double road_line1_x = 495;
	double road_line2_x = 1280;
	double backgr_x = 495;
	int prev_linepos1;
	int prev_linepos2;
	int positionx = 40;
	olc::Sprite* road_backgr1 = new olc::Sprite("test_img/road_bckgr_asph_01.png");
	olc::Sprite* road_lines1 = new olc::Sprite("test_img/road_line_white_01.png");
	olc::Sprite* road_linefield1 = new olc::Sprite("test_img/road_linefield_asph_02.png");
	olc::Sprite* car = new olc::Sprite("test_img/car_crockett.png");
	olc::Sprite* lines_backgr = new olc::Sprite("test_img/lines.png");
	olc::Sprite* black = new olc::Sprite("test_img/Black.png");
	olc::Sprite* road_path = new olc::Sprite("test_img/road_path.png");
	olc::Sprite* road_path_cl = new olc::Sprite("test_img/road_path_cl.png");
	Proxy* proxy = nullptr;
	


	bool setsleep = false;

	int i = 0;
public:
	bool OnUserCreate() override {
		
		
		context = new Context(new DrawPlayerOnFirstLine(*this));
		context2 = new Context(new DrawPlayerOnSecondLine(*this));
		Clear(olc::BLACK);
		DrawSprite(0, 520, road_backgr1, 1);
		SetPixelMode(olc::Pixel::MASK);
		DrawSprite(40, 450, car, 1);
		quicksaves = new CareTaker(&Car1);
		Car1.bot = false;
		Car2.bot = true;
		std::string type;
		std::cin >> type;
		if (type == "server") {
			proxy = new Server();
		}
		else {
			proxy = new Client();
		}
		proxy->run();
		return true;
	}
	bool OnUserUpdate(float fElapsedTime) override {
		if (setsleep) {
			Sleep(1000);
			setsleep = false;
		}
		context->draw();
		context2->draw();
		if (!(Car2.GearboxPCar(GetKey(olc::Key::SPACE).bPressed, GetKey(olc::Key::CTRL).bPressed))) {
			Car2.EnginePCar(true, true, GetKey(olc::Key::W).bReleased, fElapsedTime);
		}
		if (!(Car1.GearboxPCar(GetKey(olc::Key::SPACE).bPressed, GetKey(olc::Key::CTRL).bPressed))) {
			Car1.EnginePCar(GetKey(olc::Key::W).bPressed, GetKey(olc::Key::W).bHeld, GetKey(olc::Key::W).bReleased, fElapsedTime);
		}

		//Car1.EnginePCar(GetKey(olc::Key::W).bPressed, GetKey(olc::Key::W).bHeld, GetKey(olc::Key::W).bReleased, fElapsedTime);

		str_rpm_current = to_string(Car1.getRpm()*10000);
		str_speed_current = to_string(Car1.getSpeed());
		DrawSprite(0, 0, black, 1);
		DrawSprite(0, 80, black, 1);
		if (Car1.getRpm() <= 0.6)
			DrawString(4, 4, "RPM " + str_rpm_current, olc::WHITE, 2);
		if (Car1.getRpm() > 0.6)
			DrawString(4, 4, "RPM " + str_rpm_current, olc::RED, 2);

		if (GetKey(olc::Key::Q).bPressed) {
			Sleep(10000);
			DrawString(4, 120, "SAVING...");
			quicksaves->backup();
			setsleep = true;
		}
		if (GetKey(olc::Key::R).bPressed) {
			DrawString(4, 120, "RESTORING SAVE...");
			quicksaves->undo();
			setsleep = true;
		}
		Car1.position -= Car1.GetBackgroundSpeed(car1pos, 10);
		Car2.position = proxy->com(Car1.position);
		std::cout << Car2.position-Car1.position << std::endl;


		DrawString(4, 40, "SPEED " + str_speed_current, olc::WHITE, 2);
		DrawString(4, 84, "GEAR " + to_string(Car1.getGear()), olc::WHITE, 2);
		DrawSprite(prev_linepos1, 600, road_linefield1, 1);	
		DrawSprite(prev_linepos2, 600, road_linefield1, 1);
		DrawSprite(prev_linepos1, 700, road_path_cl, 1);
		
		DrawSprite(prev_linepos1 = road_line1_x = Car1.GetBackgroundSpeed(road_line1_x, 10), 600, road_lines1, 1);
		DrawSprite(prev_linepos2 = road_line2_x = Car1.GetBackgroundSpeed(road_line2_x, 10), 600, road_lines1, 1);
		DrawSprite(prev_linepos1, 700, road_path, 1);
		if (GetKey(olc::Key::RIGHT).bHeld) {
			++positionx;
			DrawSprite(positionx, 450, car, 1);
			DrawSprite(positionx, 550, car, 1);
		}
		if (GetKey(olc::Key::LEFT).bHeld) {
			--positionx;
			DrawSprite(positionx, 450, car, 1);
			DrawSprite(positionx + (Car2.position - Car1.position), 550, car, 1);
		}
		DrawSprite(positionx + (Car2.position - Car1.position), 550, car, 1);
		if (road_line1_x < -313)
			road_line1_x = 1260;
		if (road_line2_x < -313)
			road_line2_x = 1260;
		if (backgr_x < 0)
			backgr_x = 600;
		return true;
	}

};

int main() {
	MainWindow screen;
	if (screen.Construct(1280, 720, 1, 1)) {
		screen.Start();
	}
	return 0;
}
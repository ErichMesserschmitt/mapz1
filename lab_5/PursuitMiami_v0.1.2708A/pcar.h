
#include <string>
#include <cstdlib>
#include <stdio.h>
#include <conio.h>
#include <chrono>
#include <time.h>
#include "server.h"

class MainWindow;
class PCar;



class Mediator {
public:
	virtual void Log(PCar* base, std::string event) const = 0;
};

class ConcreteMediator {
private: PCar* pcar1;
};



class Memento {
public:
	virtual PCar* state() const = 0;
};


class ConcreteMemento : public Memento {
private:
	PCar* state_;
	~ConcreteMemento();
public:
	ConcreteMemento(const PCar& state);
	PCar* state() const override;
};


class DrawStrategy {
public:
	virtual ~DrawStrategy() {}
	virtual void drawCanvas() const = 0;
};

class Context
{
private:
	DrawStrategy* strategy_;
	
public:
	Context(DrawStrategy* strategy = nullptr) : strategy_(strategy)
	{
	}
	~Context()
	{
		delete this->strategy_;
	}
	
	void set_strategy(DrawStrategy* strategy)
	{
		delete this->strategy_;
		this->strategy_ = strategy;
	}
	void draw() const
	{
		//std::cout << "CONTEXT: Strategy method drawCanvas() called\n";
		this->strategy_->drawCanvas();
	}
};


class DrawPlayerOnFirstLine : public DrawStrategy
{
private:
	MainWindow* window;
public:
	DrawPlayerOnFirstLine(MainWindow& w) {
		window = &w;
	}

	void drawCanvas() const override
	{
		//std::cout << "STRATEGY\tDrawing bot on second line\n";
		//std::cout << "STRATEGY\tDrawing player on first line\n";
	}
};

class DrawPlayerOnSecondLine : public DrawStrategy
{
private:
	MainWindow* window;
public:
	DrawPlayerOnSecondLine(MainWindow& w) {
		window = &w;
	}
	
	void drawCanvas() const override
	{
		//std::cout << "STRATEGY\tDrawing player on second line\n";
		//std::cout << "STRATEGY\tDrawing bot on first line\n";
	}
};


class Command {
public:
	virtual ~Command() {};
	virtual void exec() const = 0;
};

class DrawCommand : public Command {
private:
	MainWindow* window;
	olc::Sprite* sprite;
	int x, y;
public:
	DrawCommand(MainWindow* w, int){
		this->window = w;
		this->sprite = sprite;
		this->x = x;
		this->y = y;
	}
	void exec() const override {
		window->DrawSprite(x, y, sprite, 1);
	}
};

class ConnectCommand : public Command {
private: std::string ip;
	   std::string type;
	   Proxy* proxy;
public:
	ConnectCommand(Proxy* proxy, std::string ip, std::string type) {
		this->ip = ip;
		this->type = type;
		this->proxy = proxy;
	}
	void exec() const override {
		proxy->run();
	}
};





class PCar {
private:
	double car_rpm_idle;
	double car_rpm_curr;
	double car_rpm_max = 0.6000;
	double car_hp;
	double car_speed_current;
	int car_gears_max = 5;
	double car_gears_coef[6] = { 0, 1.0, 0.2, 0.05, 0.02, 0.005 };
	double car_speed_coef[6] = { 0, 60, 120, 190, 210, 280 };
	int car_gears_current = 0;
	bool car_gearbox_isauto;
	double key_rpm_counter = 0.005;
	
public:
	Mediator* mediator;
	PCar() {
		
	}
	bool bot;
	double position = 0;
	double getRpm() {
		return car_rpm_curr;
	}
	double getSpeed() {
		return car_speed_current;
	}
	int getGear() {
		return car_gears_current;
	}
	bool GearboxPCar(bool bUpPressed, bool bDowPressed) {
		if (bUpPressed) {
			if (car_gears_current < car_gears_max) {
				++car_gears_current;
				key_rpm_counter = KeyRpmGearReturn(car_rpm_curr, car_gears_coef[car_gears_current]);
				return true;
			}
		}
		if (bDowPressed) {
			if (car_gears_current > 0) {
				--car_gears_current;
				key_rpm_counter = KeyRpmGearReturn(car_rpm_curr, car_gears_coef[car_gears_current]);
				return true;
			}
		}
		return false;
	}
	void EnginePCar(bool bPressed, bool bHeld, bool bReleased, float fElapsedTime) {
		if (bot) {
			if (car_rpm_max - 0.02 < car_rpm_curr) {
				if (car_gears_current <1) {
					GearboxPCar(true, false);
				}
			}
		}
		if (bHeld) {
			if (car_rpm_curr <= car_rpm_max) {
				key_rpm_counter += fElapsedTime * 0.2;
			}
			else {
				key_rpm_counter -= 0.0007;
			}
		}
		else {
			if (key_rpm_counter > 0.0010)
				key_rpm_counter -= 0.0008;
			if (!car_gears_current) {
				if (car_speed_current >= 0.01)
					car_speed_current -= 0.01;
			}
		}
		if (key_rpm_counter)
			if (car_gears_current) {
				car_rpm_curr = car_gears_coef[car_gears_current] * pow(key_rpm_counter, 1.9);
			}
			else {
				car_rpm_curr = 4 * key_rpm_counter;
			}
		if (car_rpm_curr <= 0) {
			car_rpm_curr = 0;
		}
		if (car_gears_current)
			car_speed_current = car_rpm_curr * car_speed_coef[car_gears_current];
	}
	double KeyRpmGearReturn(float car_rpm, float key_rpm_gear_koef) {
		double rpm;
		if (!car_gears_current) {
			rpm = car_rpm_curr / 4;
			return rpm;
		}
		rpm = car_speed_current / car_speed_coef[car_gears_current];
		double speed_predict = rpm * car_speed_coef[car_gears_current];

		double key_predict = pow((rpm / car_gears_coef[car_gears_current]), (1 / 1.9));
		return key_predict;
	}

	double GetBackgroundSpeed(double current_x, double coef) {
		return current_x - (car_speed_current / coef);
	}


	Memento* save() {
		std::cout << this->car_rpm_curr << std::endl;
		std::cout << this->car_speed_current << std::endl;
		std::cout << this->car_gears_current << std::endl;
		std::cout << this->position << std::endl;
		return new ConcreteMemento(*this);
	}

	void restore(Memento* memento) {
		this->car_rpm_curr = memento->state()->car_rpm_curr;
		std::cout << this->car_rpm_curr << std::endl;
		this->car_speed_current = memento->state()->car_speed_current;
		std::cout << this->car_speed_current << std::endl;
		this->car_gears_current = memento->state()->car_gears_current;
		std::cout << this->car_gears_current << std::endl;
		this->position = memento->state()->position;
		std::cout << this->position << std::endl;
		this->key_rpm_counter = memento->state()->key_rpm_counter;
		std::cout << this->key_rpm_counter << std::endl;
	}
};

	ConcreteMemento::~ConcreteMemento() {
		delete state_;
	}

	ConcreteMemento::ConcreteMemento(const PCar& state)
	{
		this->state_ = new PCar(state);
	}
	PCar* ConcreteMemento::state() const {
		return this->state_;
	}



class CareTaker : public PCar {
private:
	std::vector<Memento*> mementos;
	PCar* pcar;

public:
	CareTaker(PCar* pcar) {
		this->pcar = pcar;
	}

	void backup() {
		std::cout << "\nSaving...\n";
		this->mediator->Log(this, "Saving temporary backup...");
		this->mementos.push_back(this->pcar->save());
	}
	void undo() {
		if (!this->mementos.size()) {
			return;
		}
		Memento* memento = this->mementos.back();
		this->mementos.pop_back();
		std::cout << "Restoring..." << std::endl;
		this->pcar->restore(memento);
	}
};




#define _CRT_SECURE_NO_WARNINGS
#include "lexer.h"
#include <ctype.h>
#include <iostream>
#include <algorithm>



LexerType Lexer::analyzeToken(const std::string& token)
{
	if (token == "\n") return Colon;
	if (std::all_of(token.begin(), token.end(), ::isdigit)) return Constant;
	if (token == "?") return Condition;
	if (token == "{")  return BrOpen;
	if (token == "}") return BrClose;
	if (token == "(") return SpOpen;
	if (token == ")") return SpClose;
	if (token == "!") return Otherwise;
	if (token == "\"") return QMark;
	if (token == "LOOP") return Cycle;
	if (token == "CONNECT") return Operation_Connect;
	if (token == "CLICK") return Operation_Click;
	if (token == "CHANGE") return Operation_Change;
	if (token == "PRINT") return Operation_Print;
	if (token == "THREAD") return Operation_Thread;
	if (token == "SET") return Operation_Set;
	if (!token.empty()) return Variable;

	return None;
}

bool Lexer::checkToken(const std::string& token) {
	if (isalpha(token[0]) || isalnum(token[0])) {
		return true;
	}
	else {
		return false;
	}
}

int Lexer::findEndOfString(const std::string& code) {
	std::string m_String;
	std::string ret = code;
	int counter = 0;
	for (auto& i : code) {
		if (i != '\"') {
			m_String.push_back(i);
			++counter;
		}
		else {
			tokens.push_back(Token(LString, m_String));
			std::cout << "<" << tokens[tokens.size() - 1].type << ", " << tokens[tokens.size() - 1].text << ">" << std::endl;
			tokens.push_back(Token(QMark, std::string("\"")));
			std::cout << "<" << tokens[tokens.size() - 1].type << ", " << tokens[tokens.size() - 1].text << ">" << std::endl;
			++counter;
			break;
		}
	}
	return counter;
}

LexerType Lexer::tokinizeCode(const std::string& code)
{
	std::string newstring;
	for (auto ch : code) {
		if (ch == '\n') {
			newstring.append(" \n");
		}
		else {
			newstring += ch;
		}
	}
	uint32_t codeSize = 0;
	char* text = new char[newstring.size()];
	std::cout << newstring << std::endl;
	strcpy(text, newstring.c_str());
	char* token = text;
	token = strtok(text, lex.data());
	token = text;

	tokens.push_back(Token(analyzeToken(token), token));
	std::cout << "<" << tokens[tokens.size() - 1].type << ", " << tokens[tokens.size() - 1].text << ">" << std::endl;
	int tokensize = std::string(token).size();
	token += tokensize;
	codeSize += tokensize;
	bool markOpened = false;

	std::string temp;
	while (codeSize < newstring.size()) {



		if (checkToken(token)) {
			temp = std::string(token);
			token = strtok(token, lex.data());
			tokens.push_back(Token(analyzeToken(std::string(token)), token));

			std::cout << "<" << tokens[tokens.size() - 1].type << ", " << tokens[tokens.size() - 1].text << ">" << std::endl;
			tokensize = std::string(token).size();
			token += tokensize;
			token[0] = temp[tokensize];
			codeSize += tokensize;
		}
		else {
			if (token[0] && token[0] != ' ') {
				tokens.push_back(Token(analyzeToken(std::string(1, token[0])), std::string(1, token[0])));
				std::cout << "<" << tokens[tokens.size() - 1].type << ", " << tokens[tokens.size() - 1].text << ">" << std::endl;
			}
			codeSize += sizeof(char);
			token += sizeof(char);
		}
		if (tokens[tokens.size() - 1].type == QMark) {
			if (markOpened) {
				markOpened = false;
			}
			else {
				markOpened = true;
				temp = std::string(token);
				int a = findEndOfString(std::string(token));
				token = token + a;
				token[0] = temp[a];
				codeSize += a;
			}
		}
	}
	return LexerType();
}

#include "MyForm.h"
#include "Browser.h"
#include "parser.h"
#include <shared_mutex>
#include <msclr/marshal_cppstd.h>




using namespace System;
using namespace System::Windows::Forms;


[STAThreadAttribute]
void main() {
	Application::EnableVisualStyles();
	Application::SetCompatibleTextRenderingDefault(false);
	//WinformCDemo is your project name
	Interpretatorrelease::MyForm form;
	Application::Run(% form);
}




void Parser::setTokens(std::vector<Token> tokens) {
	this->tokens.clear();
	this->tokens = tokens;
}

void Parser::parseTokens() {
	int tokensToSkip = 0;
	for (auto i = 0; i < tokens.size(); ++i) {
		try {
			switch (tokens[i].type) {
			case Condition: {
				parseCondition();
				--i;
				break;
			}
			case Operation_Connect: {
					parseOpConnect();
					--i;
				break;
			}
			case Variable: {
				parseVariable();
				--i;
				break;
			}
			case Operation_Print: {
				parseOpPrint();
				--i;
				break;
			}
			case Operation_Set: {
				parseOpSet();
				--i;
				break;
			}
			case Operation_Thread: {
				parseOpThread();
				--i;
				break;
			}
			case Operation_Change: {
				parseOpChange();
				--i;
				break;
			}

			case Cycle: {
				parseCycle();
				--i;
				break;
			}
			case Colon: {
				++lineCounter;
				clearTokens(1);
				--i;
				break;
			}
			case Operation_Click: {
				parseOpClick();
				--i;
				break;
			}
			default:
				clearTokens(1);
				--i;
				break;
			}
		}
		catch (ParserException & e) {
			switch(e.code) {
			case 13:
				std::cout << "ERROR\t";
				break;
			case 1:
				std::cout << "WARN\t";
				break;
			}
			std::cout <<" at line " << e.line << ".\tMSG:: " << e.message << std::endl;
			break;
		}
	}
}


vector<Parser::stackElem> Parser::getStack() {
	return stack;
}

bool Parser::checkSize(size_t size, size_t currentSize) {
	if (currentSize + 1 == size) {
		return true;
	}
	return false;
}

void Parser::parseCondition() {

	int tokensCounter = 0;
	bool isWithOtherwise = false;
	bool isWithParams = true;
	int arg = 0;
	size_t tokensSize = tokens.size();
	bool isParamString = false;
	vector<Token> arguments;

	if (tokens[tokensCounter + 1].type != SpOpen) {
		throw ParserException(lineCounter, "parser.cpp::parseCondition() missing '('", 13, true);
	}
	++tokensCounter;
	if (checkSize(tokensSize, tokensCounter)) {
		throw ParserException(lineCounter, "parser.cpp::parseCondition() end of code", 13, true);
	}

	if (tokens[tokensCounter + 1].type == SpClose) {
		std::cout << "WARN\tat line " << lineCounter << " condition without params. Value checked as 'true'" << std::endl;
		isWithParams = false;
		++tokensCounter;
		if (checkSize(tokensSize, tokensCounter)) {
			throw ParserException(lineCounter, "parser.cpp::parseCondition() end of code", 13, true);
		}
	}
	else {
		if (tokens[tokensCounter + 1].type == QMark) {
			++tokensCounter;
			if (tokens[tokensCounter + 1].type != LString) {
				throw ParserException(lineCounter, "parser.cpp::parseCondition() wrong type of parameter", 13, true);
			}
			++tokensCounter;
			arg = tokensCounter;
			if (tokens[tokensCounter + 1].type != QMark) {
				throw ParserException(lineCounter, "parser.cpp::parseCondition() missing '\"'", 13, true);
			}
			isParamString = true;
		}
		else {
			if (tokens[tokensCounter + 1].type != Variable) {
				if (tokens[tokensCounter + 1].type != Constant) {
					throw ParserException(lineCounter, "parser.cpp::parseCondition() wrong type of parameter", 13, true);
				}
			}
			arg = tokensCounter + 1;
		}
		++tokensCounter;
		if (checkSize(tokensSize, tokensCounter)) {
			throw ParserException(lineCounter, "parser.cpp::parseCondition() end of code", 13, true);
		}
		if (tokens[tokensCounter + 1].type != SpClose) {
			throw ParserException(lineCounter, "parser.cpp::parseCondition() missing ')'", 13, true);
		}
		++tokensCounter;
		if (checkSize(tokensSize, tokensCounter)) {
			throw ParserException(lineCounter, "parser.cpp::parseCondition() end of code", 13, true);
		}

	}
	arguments.push_back(tokens[arg]);
	



	if (tokens[tokensCounter + 1].type != BrOpen) {
		throw ParserException(lineCounter, "parser.cpp::parseCondition() missing '{'", 13, true);
	}
	++tokensCounter;
	if (checkSize(tokensSize, tokensCounter)) {
		throw ParserException(lineCounter, "parser.cpp::parseCondition() end of code", 13, true);
	}

	vector<Token> conditionBody;
	int brcounter = 0;
	for (int i = tokensCounter + 1; i < tokens.size(); ++i) {
		if (tokens[i].type == BrOpen) {
			++brcounter;
		}
		if (tokens[i].type == BrClose) {
			if (!brcounter) {
				tokensCounter = i;
				break;
			}
			--brcounter;
		}
		conditionBody.push_back(tokens[i]);
	}
	if (brcounter) {
		std::cout << "WARN\tat line " << lineCounter << " condition with missing '}'" << std::endl;
	}

	Parser A(conditionBody);
	A.parseTokens();

	stack.push_back(stackElem(condition, ++counter, arguments, A.getStack()));


	if (tokens[tokensCounter + 1].type == Otherwise) {
		++tokensCounter;

		if (tokens[tokensCounter + 1].type != BrOpen) {
			throw ParserException(lineCounter, "parser.cpp::parseCondition() missing '{'", 13, true);
		}
		++tokensCounter;
		vector<Token> conditionBody;
		int brcounter = 0;
		for (int i = tokensCounter + 1; i < tokens.size(); ++i) {
			if (tokens[i].type == BrOpen) {
				++brcounter;
			}
			if (tokens[i].type == BrClose) {

				if (!brcounter) {
					tokensCounter = i;
					break;
				}
				--brcounter;
			}
			conditionBody.push_back(tokens[i]);
		}
		if (brcounter) {
			std::cout << "WARN\tat line " << lineCounter << " condition with missing '}'" << std::endl;
		}
		Parser A(conditionBody);
		A.parseTokens();
		if (conditionBody.empty()) {
			++tokensCounter;
			if (checkSize(tokensSize, tokensCounter)) {
				throw ParserException(lineCounter, "parser.cpp::parseCondition() end of code", 13, true);
			}
		}
		stack.push_back(stackElem(otherwise, ++counter, std::vector<Token>(), A.getStack()));
	}
	clearTokens(tokensCounter);
}

void Parser::parseVariable() {
	int tokensCounter = 0;
	size_t tokensSize = tokens.size();
	int arg = 0;
	vector<Token> arguments;
	if (checkSize(tokensSize, tokensCounter)) {
		throw ParserException(lineCounter, "parser.cpp::parseVariable() end of code", 13, true);
	}

	if (tokens[tokensCounter + 1].type == SpOpen) {
		++tokensCounter;
		if (checkSize(tokensSize, tokensCounter)) {
			throw ParserException(lineCounter, "parser.cpp::parseVariable() end of code", 13, true);
		}
		if (tokens[tokensCounter + 1].type == QMark) {
			++tokensCounter;
			if (checkSize(tokensSize, tokensCounter)) {
				throw ParserException(lineCounter, "parser.cpp::parseVariable() end of code", 13, true);
			}
			if (tokens[tokensCounter + 1].type != LString) {
				throw ParserException(lineCounter, "parser.cpp::parseVariable() wrong type of parameter", 13, true);
			}
			++tokensCounter;
			if (checkSize(tokensSize, tokensCounter)) {
				throw ParserException(lineCounter, "parser.cpp::parseVariable() end of code", 13, true);
			}
			arg = tokensCounter;
			if (tokens[tokensCounter + 1].type != QMark) {
				throw ParserException(lineCounter, "parser.cpp::parseVariable() missing '\"'", 13, true);
			}
		}
		else {
			if (tokens[tokensCounter + 1].type != Variable) {
				if (tokens[tokensCounter + 1].type != Constant) {
					throw ParserException(lineCounter, "parser.cpp::parseVariable() wrong type of parameter", 13, true);
				}
			}
			
			arg = tokensCounter + 1;
		}
		++tokensCounter;
		if (checkSize(tokensSize, tokensCounter)) {
			throw ParserException(lineCounter, "parser.cpp::parseVariable() end of code", 13, true);
		}
		arguments.push_back(tokens[0]);
	
			arguments.push_back(tokens[arg]);
	
		if (tokens[tokensCounter + 1].type != SpClose) {
			throw ParserException(lineCounter, "parser.cpp::parseVariable() missing ')'", 13, true);
		}
		++tokensCounter;
		if (checkSize(tokensSize, tokensCounter)) {
			throw ParserException(lineCounter, "parser.cpp::parseVariable() end of code", 13, true);
		}

		stack.push_back(stackElem(variableInitialize, ++counter, arguments, std::vector<Parser::stackElem>()));
	}
	else {
		variables[tokens[0].text] = std::string();
		stack.push_back(stackElem(variable, ++counter, arguments, std::vector<Parser::stackElem>()));
	}
	++tokensCounter;
	clearTokens(tokensCounter);
}

void Parser::parseCycle() {
	int tokensCounter = 0;
	size_t tokensSize = tokens.size();
	int brcounter = 0;
	std::vector<Token> arguments;
	std::vector<Token> cycleBody;

	if (checkSize(tokensSize, tokensCounter)) {
		throw ParserException(lineCounter, "parser.cpp::parseCycle() end of code", 13, true);
	}
	if (tokens[tokensCounter + 1].type != SpOpen) {
		throw ParserException(lineCounter, "parser.cpp::parseCycle() missing '('", 13, true);
	}
	++tokensCounter;
	if (checkSize(tokensSize, tokensCounter)) {
		throw ParserException(lineCounter, "parser.cpp::parseCycle() end of code", 13, true);
	}
	if (tokens[tokensCounter + 1].type != Constant) {
		throw ParserException(lineCounter, "parser.cpp::parseCycle() missing argument", 13, true);
	}
	++tokensCounter;
	arguments.push_back(tokens[tokensCounter]);
	if (checkSize(tokensSize, tokensCounter)) {
		throw ParserException(lineCounter, "parser.cpp::parseCycle() end of code", 13, true);
	}
	if (tokens[tokensCounter + 1].type != SpClose) {
		throw ParserException(lineCounter, "parser.cpp::parseCycle() missing ')'", 13, true);
	}
	++tokensCounter;

	if (checkSize(tokensSize, tokensCounter)) {
		throw ParserException(lineCounter, "parser.cpp::parseCycle() end of code", 13, true);
	}
	if (tokens[tokensCounter + 1].type != BrOpen) {
		throw ParserException(lineCounter, "parser.cpp::parseCycle() missing '{'", 13, true);
	}
	++tokensCounter;

	for (int i = tokensCounter + 1; i < tokens.size(); ++i) {
		if (tokens[i].type == BrOpen) {
			++brcounter;
		}
		if (tokens[i].type == BrClose) {
			if (!brcounter) {
				tokensCounter = i;
				break;
			}
			--brcounter;
		}
		cycleBody.push_back(tokens[i]);
	}
	Parser B(cycleBody);
	B.parseTokens();
	stack.push_back(stackElem(cycle, ++counter, arguments, B.getStack()));
	clearTokens(tokensCounter);

}



void Parser::parseOpConnect() {
	int tokensCounter = 0;
	size_t tokensSize = tokens.size();
	std::vector<Token> arguments;
	if (checkSize(tokensSize, tokensCounter)) {
		throw ParserException(lineCounter, "parser.cpp::parseOpConnect() end of code", 13, true);
	}
	
	if (tokens[tokensCounter + 1].type != SpOpen) {
		throw ParserException(lineCounter, "parser.cpp::parseOpConnect() missing '('", 13, true);
	}
	++tokensCounter;
	if (checkSize(tokensSize, tokensCounter)) {
		throw ParserException(lineCounter, "parser.cpp::parseOpConnect() end of code", 13, true);
	}

	if (tokens[tokensCounter + 1].type != QMark) {
		throw ParserException(lineCounter, "parser.cpp::parseOpConnect() missing '\'", 13, true);
	}
	++tokensCounter;
	if (checkSize(tokensSize, tokensCounter)) {
		throw ParserException(lineCounter, "parser.cpp::parseOpConnect() end of code", 13, true);
	}

	if (tokens[tokensCounter + 1].type != LString) {
		throw ParserException(lineCounter, "parser.cpp::parseOpConnect() wrong type of parameter", 13, true);
	}
	++tokensCounter;
	if (checkSize(tokensSize, tokensCounter)) {
		throw ParserException(lineCounter, "parser.cpp::parseOpConnect() end of code", 13, true);
	}
	arguments.push_back(tokens[tokensCounter]);





	if (tokens[tokensCounter + 1].type != QMark) {
		std::cout << "~13_ERR ::parser.cpp::parseOpConnect() missing '\"'";
		return;
	}
	++tokensCounter;
	if (checkSize(tokensSize, tokensCounter)) {
		throw ParserException(lineCounter, "parser.cpp::parseOpConnect() end of code", 13, true);
	}
	if (tokens[tokensCounter + 1].type != Variable) {
		throw ParserException(lineCounter, "parser.cpp::parseOpConnect() missing '\"", 13, true);
	}
	arguments.push_back(tokens[tokensCounter + 1]);
	++tokensCounter;
	if (checkSize(tokensSize, tokensCounter)) {
		throw ParserException(lineCounter, "parser.cpp::parseOpConnect() end of code", 13, true);
	}

	if (tokens[tokensCounter + 1].type != SpClose) {
		throw ParserException(lineCounter, "parser.cpp::parseOpConnect() missing ')'", 13, true);
	}

	
	++tokensCounter;
	
	stack.push_back(stackElem(connect, ++counter, arguments, std::vector<Parser::stackElem>()));

	++tokensCounter;
	clearTokens(tokensCounter);
}


void Parser::parseOpPrint() {
	int tokensCounter = 0;
	size_t tokensSize = tokens.size();
	int arg = 0;
	std::vector<Token> arguments;
	if (tokens[tokensCounter + 1].type != SpOpen) {
		throw ParserException(lineCounter, "parser.cpp::parseOpPrint() missing '('", 13, true);
	}
	++tokensCounter;
	if (checkSize(tokensSize, tokensCounter)) {
		throw ParserException(lineCounter, "parser.cpp::parseOpPrint() end of code", 13, true);
	}

	if (tokens[tokensCounter + 1].type == QMark) {
		++tokensCounter;
		if (checkSize(tokensSize, tokensCounter)) {
			throw ParserException(lineCounter, "parser.cpp::parseOpPrint() end of code", 13, true);
		}
		if (tokens[tokensCounter + 1].type != LString) {
			throw ParserException(lineCounter, "parser.cpp::parseOpPrint() wrong type of parameter", 13, true);
		}
		++tokensCounter;
		if (checkSize(tokensSize, tokensCounter)) {
			throw ParserException(lineCounter, "parser.cpp::parseOpPrint() end of code", 13, true);
		}
		arguments.push_back(tokens[tokensCounter]);
		if (tokens[tokensCounter + 1].type != QMark) {
			throw ParserException(lineCounter, "parser.cpp::parseOpPrint() missing '\"'", 13, true);
		}
		++tokensCounter;
		if (checkSize(tokensSize, tokensCounter)) {
			throw ParserException(lineCounter, "parser.cpp::parseOpPrint() end of code", 13, true);
		}
	} else {
		if (tokens[tokensCounter + 1].type != Variable) {
			if (tokens[tokensCounter + 1].type != Constant) {
				throw ParserException(lineCounter, "parser.cpp::parseOpPrint() wrong type of parameter", 13, true);
			}
		}
		++tokensCounter;
		if (checkSize(tokensSize, tokensCounter)) {
			throw ParserException(lineCounter, "parser.cpp::parseOpPrint() end of code", 13, true);
		}
		arguments.push_back(tokens[tokensCounter]);
	}

	if (tokens[tokensCounter + 1].type != SpClose) {
		throw ParserException(lineCounter, "parser.cpp::parseOpPrint() missing ')'", 13, true);
	}

	++tokensCounter;
	++tokensCounter;
	stack.push_back(stackElem(print, ++counter, arguments, std::vector<Parser::stackElem>()));
	clearTokens(tokensCounter);
}


void Parser::parseOpChange() {
	int tokensCounter = 0;
	size_t tokensSize = tokens.size();
	std::vector<Token> arguments;
	if (checkSize(tokensSize, tokensCounter)) {
		throw ParserException(lineCounter, "parser.cpp::parseOpChange() end of code", 13, true);
	}
	if (tokens[tokensCounter + 1].type != SpOpen) {
		throw ParserException(lineCounter, "parser.cpp::parseOpChange() missing '('", 13, true);
	}
	++tokensCounter;
	if (checkSize(tokensSize, tokensCounter)) {
		throw ParserException(lineCounter, "parser.cpp::parseOpChange() end of code", 13, true);
	}
	if (tokens[tokensCounter + 1].type != QMark) {
		throw ParserException(lineCounter, "parser.cpp::parseOpChange() missing '\"'", 13, true);
	}
	++tokensCounter;
	if (checkSize(tokensSize, tokensCounter)) {
		throw ParserException(lineCounter, "parser.cpp::parseOpChange() end of code", 13, true);
	}
	if (tokens[tokensCounter + 1].type != LString) {
		throw ParserException(lineCounter, "parser.cpp::parseOpChange() missing argument 1", 13, true);
	}
	++tokensCounter;
	arguments.push_back(tokens[tokensCounter]);
	if (checkSize(tokensSize, tokensCounter)) {
		throw ParserException(lineCounter, "parser.cpp::parseOpChange() end of code", 13, true);
	}
	if (tokens[tokensCounter + 1].type != QMark) {
		throw ParserException(lineCounter, "parser.cpp::parseOpChange() missing '\"'", 13, true);
	}
	++tokensCounter;
	if (checkSize(tokensSize, tokensCounter)) {
		throw ParserException(lineCounter, "parser.cpp::parseOpChange() end of code", 13, true);
	}
	if (tokens[tokensCounter + 1].type != QMark) {
		throw ParserException(lineCounter, "parser.cpp::parseOpChange() missing '\"'", 13, true);
	}
	++tokensCounter;
	if (checkSize(tokensSize, tokensCounter)) {
		throw ParserException(lineCounter, "parser.cpp::parseOpChange() end of code", 13, true);
	}
	if (tokens[tokensCounter + 1].type != LString) {
		throw ParserException(lineCounter, "parser.cpp::parseOpChange() missing argument 2", 13, true);
	}
	++tokensCounter;
	arguments.push_back(tokens[tokensCounter]);

	if (checkSize(tokensSize, tokensCounter)) {
		throw ParserException(lineCounter, "parser.cpp::parseOpChange() end of code", 13, true);
	}
	if (tokens[tokensCounter + 1].type != QMark) {
		throw ParserException(lineCounter, "parser.cpp::parseOpChange() missing '\"'", 13, true);
	}
	++tokensCounter;
	if (checkSize(tokensSize, tokensCounter)) {
		throw ParserException(lineCounter, "parser.cpp::parseOpChange() end of code", 13, true);
	}

	if (tokens[tokensCounter + 1].type != Variable) {
		throw ParserException(lineCounter, "parser.cpp::parseOpChange() missing argument 3", 13, true);
	}
	++tokensCounter;
	arguments.push_back(tokens[tokensCounter]);
	if (checkSize(tokensSize, tokensCounter)) {
		throw ParserException(lineCounter, "parser.cpp::parseOpChange() end of code", 13, true);
	}
	if (tokens[tokensCounter + 1].type != SpClose) {
		throw ParserException(lineCounter, "parser.cpp::parseOpChange() missing ')'", 13, true);
	}
	++tokensCounter;
	stack.push_back(stackElem(change, ++counter, arguments, std::vector<stackElem>()));
	clearTokens(tokensCounter);

}


void Parser::parseOpClick() {
	int tokensCounter = 0;
	size_t tokensSize = tokens.size();
	std::vector<Token> arguments;
	if (checkSize(tokensSize, tokensCounter)) {
		throw ParserException(lineCounter, "parser.cpp::parseOpClick() end of code", 13, true);
	}
	if (tokens[tokensCounter + 1].type != SpOpen) {
		throw ParserException(lineCounter, "parser.cpp::parseOpClick() missing '('", 13, true);
	}
	++tokensCounter;
	if (checkSize(tokensSize, tokensCounter)) {
		throw ParserException(lineCounter, "parser.cpp::parseOpClick() end of code", 13, true);
	}
	if (tokens[tokensCounter + 1].type != QMark) {
		throw ParserException(lineCounter, "parser.cpp::parseOpClick() missing '\"'", 13, true);
	}
	++tokensCounter;
	if (checkSize(tokensSize, tokensCounter)) {
		throw ParserException(lineCounter, "parser.cpp::parseOpClick() end of code", 13, true);
	}
	if (tokens[tokensCounter + 1].type != LString) {
		throw ParserException(lineCounter, "parser.cpp::parseOpClick() missing argument 1", 13, true);
	}
	++tokensCounter;
	arguments.push_back(tokens[tokensCounter]);

	if (checkSize(tokensSize, tokensCounter)) {
		throw ParserException(lineCounter, "parser.cpp::parseOpClick() end of code", 13, true);
	}
	if (tokens[tokensCounter + 1].type != QMark) {
		throw ParserException(lineCounter, "parser.cpp::parseOpClick() missing '\"'", 13, true);
	}
	++tokensCounter;
	if (checkSize(tokensSize, tokensCounter)) {
		throw ParserException(lineCounter, "parser.cpp::parseOpClick() end of code", 13, true);
	}

	if (tokens[tokensCounter + 1].type != Variable) {
		throw ParserException(lineCounter, "parser.cpp::parseOpClick() missing argument 2", 13, true);
	}
	++tokensCounter;
	arguments.push_back(tokens[tokensCounter]);
	if (checkSize(tokensSize, tokensCounter)) {
		throw ParserException(lineCounter, "parser.cpp::parseOpClick() end of code", 13, true);
	}
	if (tokens[tokensCounter + 1].type != SpClose) {
		throw ParserException(lineCounter, "parser.cpp::parseOpClick() missing ')'", 13, true);
	}
	++tokensCounter;
	stack.push_back(stackElem(click, ++counter, arguments, std::vector<stackElem>()));
	clearTokens(tokensCounter);
}



void Parser::parseOpSet() {
	int tokensCounter = 0;
	size_t tokensSize = tokens.size();
	std::vector<Token> arguments;
	if (tokens[tokensCounter + 1].type != Variable) {
		throw ParserException(lineCounter, "parser.cpp::parseOpSet() wrong type of parameter", 13, true);
	}
	++tokensCounter;
	if (checkSize(tokensSize, tokensCounter)) {
		throw ParserException(lineCounter, "parser.cpp::parseOpSet() end of code", 13, true);
	}

	arguments.push_back(tokens[tokensCounter]);
	if (tokens[tokensCounter + 1].type == QMark) {
		++tokensCounter;
		if (checkSize(tokensSize, tokensCounter)) {
			throw ParserException(lineCounter, "parser.cpp::parseOpSet() end of code", 13, true);
		}
		if (tokens[tokensCounter + 1].type != LString) {
			throw ParserException(lineCounter, "parser.cpp::parseOpSet() wrong type of parameter 2", 13, true);
		}
		++tokensCounter;
		if (checkSize(tokensSize, tokensCounter)) {
			throw ParserException(lineCounter, "parser.cpp::parseOpSet() end of code", 13, true);
		}
		arguments.push_back(tokens[tokensCounter]);
		if (tokens[tokensCounter + 1].type != QMark) {
			throw ParserException(lineCounter, "parser.cpp::parseOpSet() missing '\"'", 13, true);
		}
		++tokensCounter;
		


	} else {
		if (tokens[tokensCounter + 1].type != Constant) {
			if (tokens[tokensCounter + 1].type != Variable) {
				throw ParserException(lineCounter, "parser.cpp::parseOpSet() wrong type of parameter 2", 13, true);
			}
		}
		++tokensCounter;
		
		arguments.push_back(tokens[tokensCounter]);
	}

	stack.push_back(stackElem(set, ++counter, arguments, std::vector<Parser::stackElem>()));
	++tokensCounter;
	

	clearTokens(tokensCounter);
}

void Parser::parseOpThread() {
	int tokensCounter = 0;
	size_t tokensSize = tokens.size();
	std::vector<Token> arguments;
	std::vector<Token> threadBody;
	int brcounter = 0;
	if (tokens[tokensCounter + 1].type != BrOpen) {
		throw ParserException(lineCounter, "parser.cpp::parseOpThread() missing '{'", 13, true);
	}
	++tokensCounter;
	if (checkSize(tokensSize, tokensCounter)) {
		throw ParserException(lineCounter, "parser.cpp::parseOpThread() end of code", 13, true);
	}


	for (int i = tokensCounter + 1; i < tokens.size(); ++i) {
		if (tokens[i].type == BrOpen) {
			++brcounter;
		}
		if (tokens[i].type == BrClose) {
			if (!brcounter) {
				tokensCounter = i;
				break;
			}
			--brcounter;
		}
		threadBody.push_back(tokens[i]);
	}

	Parser B(threadBody);
	B.parseTokens();
	stack.push_back(Parser::stackElem(thread, ++counter, arguments, B.getStack()));
	clearTokens(tokensCounter);

}


void Parser::clearTokens(int count) {
	vector<Token> newTokens;
	for (int i = count; i < tokens.size(); ++i) {
		newTokens.push_back(tokens[i]);
	}
	tokens = newTokens;
}


std::string Parser::getLexerType(const LexerType& a) {
	switch (a) {
	case Variable:
		return "VAR";
	case Constant:
		return "CONST";
	case BrOpen:
		return "BROPEN '{'";
	case BrClose:
		return "BRCLOSE '}'";
	case SpOpen:
		return "BROPEN '('";
	case SpClose:
		return "BRCLOSE ')'";
	case Cycle:
		return "LOOP";
	case Operation_Set:
		return "SET";
	case Operation_Connect:
		return "CONNECT";
	case Operation_Print:
		return "PRINT";
	case Operation_Thread:
		return "THREAD";
	case Operation_Click:
		return "CLICK";
	case Operation_Change:
		return "CHANGE";
	case Condition:
		return "IF";
	case Otherwise:
		return "ELSE";
	case QMark:
		return "Qmark \"";
	case LString:
		return "STRING";
	default:
		return "";
	}
}

std::string Parser::getStackType(const elemType& a) {
	switch (a) {
	case condition:
		return "IF";
		break;
	case otherwise:
		return "ELSE";
		break;
	case set:
		return "SET_VAR";
		break;
	case thread:
		return "THREAD";
		break;
	case click:
		return "CLICK";
		break;
	case cycle:
		return "LOOP";
		break;
	case change:
		return "CHANGE";
		break;
	case print:
		return "PRINT";
		break;
	case connect:
		return "CONNECT";
		break;
	case variable:
		return "VARIABLE";
		break;
	case variableInitialize:
		return "VARIABLE_INIT";
		break;
	}
}

void Parser::getTokensTree(std::vector<stackElem> stack, int tabCount) {
	for (auto a : stack) {
		for (int i = 0; i < tabCount; ++i) {
			std::cout << "\t";
		}
		std::cout << "#" << a.number;
		std::cout << "\tType " << getStackType(a.type) << std::endl;

		if (a.arguments.size() > 0) {
			int argC = 0;
			for (auto arg : a.arguments) {
				for (int i = 0; i < tabCount; ++i) {
					std::cout << "\t";
				}
				std::cout << "  arg.: " << ++argC << "\ttype " << getLexerType(arg.type) << "\tvalue " << arg.text << std::endl;
				if (arg.type == variable) {
					for (int i = 0; i < tabCount; ++i) {
						std::cout << "\t";
					}
					std::cout << "  var_val.: " << variables[arg.text] << std::endl;
				}
			}
		}
		if (a.subStack.size() > 0) {
			getTokensTree(a.subStack, tabCount + 1);
		}
	}

}

void Parser::performer(vector<stackElem> stack, bool isThread) {
	bool condition_true = false;
	for (auto& el : stack) {
		switch (el.type) {
		case condition: {
			if (!el.arguments[0].text.empty() && el.arguments[0].type == LString) {
				performer(el.subStack);
				condition_true = true;
			}
			if (!el.arguments[0].text.empty() && (el.arguments[0].text[0] != '0') && el.arguments[0].type == Constant) {
				performer(el.subStack);
				condition_true = true;
			}
			if (!el.arguments[0].text.empty() && (el.arguments[0].text[0] != '0') && el.arguments[0].type == Variable) {
				if (!variables[el.arguments[0].text].empty() && variables[el.arguments[0].text] != "0") {
					performer(el.subStack);
					condition_true = true;
				}
			}
			break;
		}
		case otherwise: {
			if (!condition_true) {
				performer(el.subStack);
				condition_true = false;
			}
			break;
		}
		case set: {
			if (el.arguments[1].type == Variable) {
				variables[el.arguments[0].text] = variables[el.arguments[1].text];
			}
			else {
				variables[el.arguments[0].text] = el.arguments[1].text;
			}
			break;
		}

		case thread: {
			std::thread m;
			m = std::thread(&Parser::performer, this, el.subStack, true);
			m.detach();
			std::cout << m.get_id() << std::endl;
			break;
		}

		case connect: {
			std::string uri = "http://";
			uri.append(el.arguments[0].text);
			bool isCreated = false;
			System::Windows::Forms::Form^ my = Interpretatorrelease::Application::OpenForms["myForm"];
			for (auto name : handleNames) {
				std::cout << name << std::endl;
				if (el.arguments[1].text == name) {
					isCreated = true;
					break;
				}
			}
			if (!isCreated) {
				((Interpretatorrelease::MyForm^) my)->Invoke(
					gcnew System::Action<Interpretatorrelease::ArgWebBrowser^>(
						((Interpretatorrelease::MyForm^) my), &Interpretatorrelease::MyForm::createBrowserWindow), gcnew Interpretatorrelease::ArgWebBrowser(gcnew System::Uri(gcnew String(uri.c_str())), gcnew String(el.arguments[1].text.c_str())));
			}
			else {
				((Interpretatorrelease::MyForm^) my)->Invoke(
					gcnew System::Action<Interpretatorrelease::ArgWebBrowser^>(
					((Interpretatorrelease::MyForm^) my), &Interpretatorrelease::MyForm::changeBrowserUrl), gcnew Interpretatorrelease::ArgWebBrowser(gcnew System::Uri(gcnew String(uri.c_str())), gcnew String(el.arguments[1].text.c_str())));
			}
			break;
		}
		case print: {
			std::cout << variables[el.arguments[0].text] << std::endl;
			break;
		}

		case cycle: {
			for (int i = 0; i < std::stoi(el.arguments[0].text); ++i) {
				performer(el.subStack);
			}
			break;
		}

		case change: {
			System::Windows::Forms::Form^ my = Interpretatorrelease::Application::OpenForms["myForm"];
			((Interpretatorrelease::MyForm^) my)->Invoke(
				gcnew System::Action<Interpretatorrelease::ArgHtmlElement^>(
				((Interpretatorrelease::MyForm^) my), &Interpretatorrelease::MyForm::changeElementContent), gcnew Interpretatorrelease::ArgHtmlElement(gcnew String(el.arguments[2].text.c_str()), gcnew String(el.arguments[1].text.c_str()), gcnew String(el.arguments[0].text.c_str())));

			break;
		}

		case variable: {
			break;
		}
		case variableInitialize: {
			if (el.arguments[1].type == Variable) {
				variables[el.arguments[0].text] = variables[el.arguments[1].text];
			}
			else {
				variables[el.arguments[0].text] = el.arguments[1].text;
			}
			break;
		}

		case click: {
			System::Windows::Forms::Form^ my = Interpretatorrelease::Application::OpenForms["myForm"];
			((Interpretatorrelease::MyForm^) my)->Invoke(
				gcnew System::Action<Interpretatorrelease::ArgClickButton^>(
				((Interpretatorrelease::MyForm^) my), &Interpretatorrelease::MyForm::clickButton), gcnew Interpretatorrelease::ArgClickButton(gcnew String(el.arguments[1].text.c_str()), gcnew String(el.arguments[0].text.c_str())));

			break;
		}
		}
	}
	if (isThread) {
	}
}

#ifndef PARSER_H
#define PARSER_H


#include "lexer.h"
#include "connector.h"

#include <iostream>
#include <vector>
#include <map>
#include <string>

#include <thread>


using namespace std;

class ParserException {
public:
	ParserException(int line, std::string message, int code, bool isCritical) {
		this->line = line;
		this->message = message;
		this->code = code;
		this->isCritical = isCritical;
	}
	int line;
	std::string message;
	int code;
	bool isCritical;
};

class Parser {
private:
	enum elemType {
		condition = 0,
		otherwise,
		set,
		read,
		thread,
		click,
		cycle,
		change,
		print,
		connect,
		variable,
		handle,
		variableInitialize
	};

	
	map<std::string, std::string> variables;
	int handleCount = 0;

	vector<Token> tokens;
	std::string text;
	struct stackElem {
		stackElem(elemType type, int number, vector<Token> arguments, vector<stackElem> subStack) {
			this->type = type;
			this->number = number;
			this->arguments = arguments;
			this->subStack = subStack;
		}

		elemType type;
		int number = 0;
		vector<Token> arguments;
		vector<stackElem> subStack;
		
	};
	vector<stackElem> stack;
	int lineCounter = 1;
	int counter = 0;
	void clearTokens(int count);
	
	void parseCondition();
	void parseCycle();
	void parseOpPrint();
	void parseOpSet();
	void parseOpThread();
	void parseOpConnect();
	void parseOpClick();
	void parseOpChange();
	void parseVariable();

	bool checkSize(size_t size, size_t currentSize);
	std::string getLexerType(const LexerType& a);
	std::string getStackType(const elemType& a);
public:

	Parser() {
		counter = 0;
		lineCounter = 0;
		text = "";
	}
	


	Parser(vector<Token> tokens) {
		this->tokens = tokens;
	}
	void getTokensTree(std::vector<stackElem> stack, int tabCount);
	void parseTokens();
	void setTokens(std::vector<Token>);
	void performer(vector<stackElem> stack, bool isThread = false);

	vector<stackElem> getStack();
	
};


#endif
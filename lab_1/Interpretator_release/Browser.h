#pragma once
#include <iostream>
#include <msclr/marshal_cppstd.h>

namespace Interpretatorrelease {

	public ref class ArgHtmlElement {
	public:
		ArgHtmlElement(System::String^ name, System::String^ data, System::String^ doc) {
			this->data = data;
			this->name = name;
			this->doc = doc;
		}
		ArgHtmlElement(){}
		System::String^ name;
		System::String^ data;
		System::String^ doc;
	};
	public ref class ArgClickButton {
	public:
		ArgClickButton(System::String^ name,  System::String^ doc) {
			this->name = name;
			this->doc = doc;
		}
		ArgClickButton() {}
		System::String^ name;
		System::String^ doc;
	};

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for Browser
	/// </summary>
	public ref class Browser : public System::Windows::Forms::Form
	{
	public:
		Browser(void)
		{
			InitializeComponent();
			
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Browser()
		{
			if (components)
			{
				delete components;
			}
		}
	public: System::Windows::Forms::WebBrowser^ webBrowser;
	public: ArgHtmlElement A;
	public: ArgClickButton B;
	public: System::Void onCompletedDoc(System::Object^ sender, System::Windows::Forms::WebBrowserDocumentCompletedEventArgs^ e) {
		this->webBrowser->Document->GetElementById(A.doc)->InnerHtml = A.data;
	}

	public: System::Void onCompletedClick(System::Object^ sender, System::Windows::Forms::WebBrowserDocumentCompletedEventArgs^ e) {
		this->webBrowser->Document->GetElementById(A.doc)->InvokeMember("click");
	}
	protected:

	protected:

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->webBrowser = (gcnew System::Windows::Forms::WebBrowser());
			this->SuspendLayout();
			// 
			// webBrowser
			// 
			this->webBrowser->Dock = System::Windows::Forms::DockStyle::Fill;
			this->webBrowser->Location = System::Drawing::Point(0, 0);
			this->webBrowser->MinimumSize = System::Drawing::Size(20, 20);
			this->webBrowser->Name = L"webBrowser";
			this->webBrowser->Size = System::Drawing::Size(939, 672);
			this->webBrowser->TabIndex = 0;
			// 
			// Browser
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(8, 16);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(939, 672);
			this->Controls->Add(this->webBrowser);
			this->Name = L"Browser";
			this->Text = L"Browser";
			this->ResumeLayout(false);

		}
#pragma endregion
	};
}

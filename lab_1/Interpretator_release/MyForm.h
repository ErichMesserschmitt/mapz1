#pragma once
#include "parser.h"
#include "Browser.h"

#include <msclr/marshal_cppstd.h>
#include <map>
#include <shared_mutex>

std::vector<std::string> handleNames;
Parser C;


namespace Interpretatorrelease {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;


	/// <summary>
	/// Summary for MyForm
	/// </summary>
	public ref class ArgWebBrowser {
	public:
		ArgWebBrowser(System::Uri^ uri, System::String^ name) {
			this->uri = uri;
			this->name = name;
		}
		System::Uri^ uri;
		System::String^ name;
	};

	//public ref class ArgHtmlElement {
	//public:
	//	ArgHtmlElement(System::String^ name, System::String^ data, System::String^ doc) {
	//		this->data = data;
	//		this->name = name;
	//		this->doc = doc;
	//	}
	//	System::String^ name;
	//	System::String^ data;
	//	System::String^ doc;
	//};


	public ref class MyForm : public System::Windows::Forms::Form
	{
	public:
		MyForm(void)
		{
			InitializeComponent();
			this->Name = "myForm";
			this->CheckForIllegalCrossThreadCalls = false;
			//
			//TODO: Add the constructor code here
			//
		}
		
		MyForm^ getForm() {
			return this;
		}
	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~MyForm()
		{
			if (components)
			{
				delete components;
			}
		}

	private: System::Windows::Forms::RichTextBox^ richTextBox1;
	public: System::Windows::Forms::Button^ button1;
	private: 
		
	

	
	
	public: System::Void changeElementContent(ArgHtmlElement^ a) {
		System::Windows::Forms::Form^ f = Interpretatorrelease::Application::OpenForms[a->name];
		((Interpretatorrelease::Browser^)f)->A.doc = a->doc;
		((Interpretatorrelease::Browser^)f)->A.data = a->data;
		((Interpretatorrelease::Browser^)f)->A.name = a->name;
		System::Windows::Forms::WebBrowser^ c = ((Interpretatorrelease::Browser^)f)->webBrowser;
		c->DocumentCompleted += gcnew WebBrowserDocumentCompletedEventHandler(((Interpretatorrelease::Browser^)f), &Interpretatorrelease::Browser::onCompletedDoc);
	}
	public: System::Void clickButton(ArgClickButton^ a) {
		System::Windows::Forms::Form^ f = Interpretatorrelease::Application::OpenForms[a->name];
		((Interpretatorrelease::Browser^)f)->A.doc = a->doc;
		((Interpretatorrelease::Browser^)f)->A.name = a->name;
		System::Windows::Forms::WebBrowser^ c = ((Interpretatorrelease::Browser^)f)->webBrowser;
		c->DocumentCompleted += gcnew WebBrowserDocumentCompletedEventHandler(((Interpretatorrelease::Browser^)f), &Interpretatorrelease::Browser::onCompletedClick);
	}



	public: System::Void createBrowserWindow(ArgWebBrowser^ a) {
		Control::CheckForIllegalCrossThreadCalls = false;
		Interpretatorrelease::Browser^ browser = gcnew Interpretatorrelease::Browser();
		browser->Name = a->name;
		handleNames.push_back(msclr::interop::marshal_as<std::string>(Convert::ToString(a->name)));
		browser->webBrowser->Navigate(a->uri);
		browser->Show();
	}

	public: System::Void changeBrowserUrl(ArgWebBrowser^ a) {
		System::Windows::Forms::Form^ f = Interpretatorrelease::Application::OpenForms[a->name];
		((Interpretatorrelease::Browser^)f)->webBrowser->Navigate(a->uri);
	}
	protected:

		

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->richTextBox1 = (gcnew System::Windows::Forms::RichTextBox());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->SuspendLayout();
			// 
			// richTextBox1
			// 
			this->richTextBox1->Location = System::Drawing::Point(2, 2);
			this->richTextBox1->Name = L"richTextBox1";
			this->richTextBox1->Size = System::Drawing::Size(1423, 655);
			this->richTextBox1->TabIndex = 0;
			this->richTextBox1->Text = L"";
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(2, 663);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(75, 23);
			this->button1->TabIndex = 1;
			this->button1->Text = L"button1";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &MyForm::button1_Click);
			// 
			// MyForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(8, 16);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(1428, 705);
			this->Controls->Add(this->button1);
			this->Controls->Add(this->richTextBox1);
			this->Name = L"MyForm";
			this->Text = L"MyForm";
			this->ResumeLayout(false);

		}
#pragma endregion
	private: System::Void button1_Click(System::Object^ sender, System::EventArgs^ e) {

	Lexer A;
	std::string code = "";
	code = msclr::interop::marshal_as<std::string>(richTextBox1->Text);
	A.tokinizeCode(code);
	Parser C;
	std::vector<std::string> handleNames;
	std::vector<Token> tokens = A.getTokens();
	C.setTokens(tokens);
	C.parseTokens();
	C.performer(C.getStack());
	C.getTokensTree(C.getStack(), 0);
	}
	};
}

#pragma once
#include <string>
#include <vector>
#include <map>

enum LexerType {
	Variable = 0,
	Constant,
	BrOpen,
	BrClose,
	SpOpen,
	SpClose,
	Cycle,
	Operation_Set,
	Operation_Connect,
	Operation_Print,
	Operation_Thread,
	Operation_Click,
	Operation_Change,
	Condition,
	Otherwise,
	QMark,
	LString,
	Cl,
	Colon,
	None
};


class Token {
public:
	Token(LexerType type, std::string text){
		this->type = type;
		this->text = text;
	}
	LexerType type;
	std::string text;
};

class Lexer {
public:
	Lexer() {};
	~Lexer() {};
private:
	std::string lex = "\n { } ( ) ? !  ; \" ";
	std::vector<Token> tokens;
public:

	LexerType analyzeToken(const std::string& token);
	bool checkToken(const std::string& token);
	LexerType tokinizeCode(const std::string& code);
	int findEndOfString(const std::string& code);
	std::vector<Token> getTokens() { return tokens; }


};

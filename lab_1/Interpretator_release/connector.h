#ifndef CONNECTOR_H
#define CONNECTOR_H

#define FILE_SIZE 4096

#include <urlmon.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>

std::string connectToSite(std::string urlAddress);


#endif